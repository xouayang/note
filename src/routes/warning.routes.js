const controller = require("../controller/warning.controller");
const {verifyToken} = require('../middleware/verifyToken')
module.exports = (app) => {
  app.get("/warning",verifyToken,controller.warning);
  app.put("/close-account/:id",controller.close_account);
};