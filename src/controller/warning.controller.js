const sql = require("mssql");
const db = require("../config/db");
const { status_Code } = require("../helper/status_code");
exports.warning = async (req, res) => {
  try {
    const { MEMBER_ID } = req.payload;
    const connection = await sql.connect(db);
    const query = await connection.request().query(`SELECT * FROM Tb_Note_detail
        WHERE CONVERT(DATETIME, expired_date,103) <= GETDATE() + 12 AND status = '1'
          AND in_contact_or_action = '0' AND member_id_create = '${MEMBER_ID}'`);
    if (!query) {
      return res
        .status(status_Code.NotFound)
        .json({ message: "Not found data" });
    } else {
      return res.status(status_Code.success).json(query.recordset);
    }
  } catch (error) {
    return res
      .status(status_Code.Server_error)
      .json({ message: error.message });
  }
};
// close account
exports.close_account = async (req, res) => {
  try {
    const { id } = req.params;
    const { status } = req.body;
    const connection = await sql.connect(db);
    const query = connection
      .request()
      .query(
        `UPDATE Tb_Note_detail SET status='${status}' WHERE note_id = '${id}'`
      );
    if (query) {
      return res.status(status_Code.success).json({ message: "Closed" });
    }
  } catch (error) {
    return res
      .status(status_Code.Server_error)
      .json({ message: error.message });
  }
};
// warning to contact
exports.warning_contact = async (req, res) => {
  try {
    const { id } = req.params;
    const { in_contact_or_action } = req.body;
    const connection = await sql.connect(db);
    const query = await connection.request().query(`
   UPDATE Tb_Note_detail SET in_contact_or_action = '${in_contact_or_action}' WHERE note_id = '${id}' `);
  } catch (error) {
    return res
      .status(status_Code.Server_error)
      .json({ message: error.message });
  }
};
